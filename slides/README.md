---
marp: true
theme: gaia
class:
  - lead
  - invert
paginate: true

---
<!-- _paginate: false -->

# **21** motivos para usar **Software Livre** em 20**21**
Autor: Samuel Gonçalves

---
<!-- _class: lead -->
![bg right:40% 120%](images/bernard.jpg)
## *"Liberdade implica responsabilidade, por isso tantos a temem!"*

*Bernard Shaw*

---
![bg right:30% 80%](images/perfil.png)

#### 🇧🇷 **Samuel Gonçalves Pereira**

* Consultor de Tecnologia de **Segurança Ofensiva** e **DevSecOps**
* Mais de 10 anos de experiência
* Mais de 2 mil alunos ensinados
* Músico, Contista, YouTuber e Podcaster

---

## Entre em contato
[https://beacons.ai/sgoncalves](https://beacons.ai/sgoncalves)
![bg left:50% 70%](images/qr_beacons_sgoncalves.png)

---

## Estes slides são **OpenSource**!!
![bg right:50% 70%](images/qr_slides_21motivos.png)
### É só baixar e usar!
---

### **SORTEIO NO FIM DA PALESTRA!**
![bg right:50% 80%](images/qr-sorteio.png)
##### Inscreva-se e participe!
Escaneie o QRCode ou [clique aqui!](https://sorteio.4linux.com.br/21-motivos-para-usar-software-livre-em-2021)

---

## **Por que Software Livre?**
![bg 120% blur:5px](images/bg-source-code.jpg)

---
<!-- _class: lead -->

# Motivo #1
## Você **pode** executar o programa como desejar, para **qualquer** propósito
![bg left:35%](images/bg-01-code.jpg)

---

# Motivo #2
## Você **pode** estudar como o programa funciona, e adaptá-lo às suas necessidades
![bg right:33%](images/bg-02-person-code.jpg)

---
<!-- _class: lead -->

# Motivo #3
## Você **pode** redistribuir cópias do software livremente
![bg left:35%](images/bg-03.jpg)

---

# Motivo #4
## Você **pode** distribuir cópias de suas versões **modificadas**, de modo que possa **ajudar** outros
![bg right:35%](images/bg-04.jpg)

---
<!-- _class: lead -->

# Motivo #5
## Ao contrário da maioria das profissões, o talento de código aberto é escasso e em alta demanda.
###### *E está amplamente documentado na internet!*
![bg left:35%](images/bg-05.jpg)

---

# Motivo #6
## A cultura do OpenSource permite que equipes colaborem mais entre si, tornando o desenvolvimento mais ágil e seguro
![bg right:35%](images/bg-06.jpg)

---
<!-- _class: lead -->

# Motivo #7
## Diversos países ao redor do Planeta tem adotado FOSS como padrão e incentivado seu uso na indústria
![bg left:35%](images/bg-07.jpg)

---

# Motivo #8
## Estratégias de modernização da infraestrutura constituem o uso principal do open source empresarial
![bg right:35%](images/bg-08.jpg)

---
<!-- _class: lead -->

# Motivo #9
## O mercado global de serviços de código aberto deve crescer **18,2%** até 2026, movimentando mais de **US$ 50 bilhões** até 2026, e mais de **US$ 21,7 bilhões** ainda em 2021.
![bg left:35%](images/bg-money.jpg)

---

# Motivo #10
## Segundo a **RedHat** 90% dos líderes de TI usam soluções open source empresariais.
![bg right:35%](images/bg-stats.jpg)

---
<!-- _class: lead -->

# Motivo #11
## Software Livre **Estimula a inovação e cultiva uma melhor aprendizagem**
![bg left:35%](images/bg-study.jpg)

---

# Motivo #12
## **Pode** promover a inclusão digital de forma econômica e eficiente
![bg right:35%](images/bg-inclusion.jpg)

---
<!-- _class: lead -->

# Motivo #13
#### Possibilita a independência do fornecedor já que o contratante não fica dependente de apenas um fabricante, tão pouco obrigado a adquirir novas licenças a cada vez que algum software deixa de ter suporte ou lança-se uma nova versão
![bg left:35%](images/bg-04.jpg)

---

# Motivo #14
## Intensifica a segurança da informação, pois, com o software aberto é possível identificar melhor possíveis falhas, de maneira mais rápida e eficiente
![bg right:35%](images/bg-hacker.jpg)

---
<!-- _class: lead -->

# Motivo #15
## Softwares **OpenSource** são o **motor** das transformações digitais
![bg left:35%](images/bg-10.jpg)

---

# Motivo #16
## Áreas relacionadas a **edge computing** e **IA/ML** são grandes produtores e consumidores de tecnologia **Open Source**, com crescimento superior a 20%, previsto para os próximos 2 anos
![bg right:35%](images/bg-source-code.jpg)

---
<!-- _class: lead -->

# Motivo #17
## O uso de tecnologias abertas para gerenciamento de containers cresce exponencialmente, podendo chegar a 72% nos próximos 2 anos
![bg left:35%](images/bg-stats.jpg)

---

# Motivo #18
## Colaboração é parte integrante da vida humana, e graças a ela chegamos até aqui
![bg right:35%](images/bg-inclusion.jpg)

---
<!-- _class: lead -->

# Motivo #19
## Você **pode** ser protagonista desta história!
![bg left:40%](images/bg-you.jpg)

---

# Motivo #20
## Os processos se tornam muito mais transparentes e participativos
![bg right:35%](images/bg-08.jpg)

---

# **Motivo #21**
## Comunidades de Software Livre criaram uma nova maneira de colaboração, descentralizada e sem a necessidade de coordenação, favorecendo a **democracia**!
![bg blur:5px](images/democracy.jpg)

---

## "Quanto mais ideias há em circulação, mais ideias há para qualquer indivíduo discordar delas."
*Clay Shirky*

---

### Recomendação
[Clay Shirky: Como a internet irá (um dia) transformar o governo](https://www.ted.com/talks/clay_shirky_how_the_internet_will_one_day_transform_government?language=pt-br)
![bg left:60%](images/ClayShirky.jpg)

---
<!-- _class: lead -->
## **Obrigado!**

Vamos nos conectar?
* **Site:** [sgoncalves.tec.br](https://sgoncalves.tec.br)
* **E-mail:** [samuel@sgoncalves.tec.br](https://sgoncalves.tec.br/contato)
* **Linkedin:** [linkedin.com/in/samuelgoncalvespereira/](linkedin.com/in/samuelgoncalvespereira/)
* **Telegram:** [t.me/Samuel_gp](t.me/Samuel_gp)
* **Todas as redes:** [https://beacons.ai/sgoncalves](https://beacons.ai/sgoncalves)

---
<style scoped>
  p {
  font-size: 22pt;
  list-style-type: circle;
}
</style>

### Referências Bibliográficas
[O que é software livre?](https://www.gnu.org/philosophy/free-sw.pt-br.html)
[O Estado do Open Source Empresarial](https://www.redhat.com/pt-br/enterprise-open-source-report/2021)
[Open Source: O motor que está alavancando o crescimento da cloud](https://www.ascenty.com/blog/artigos/open-source-o-motor-que-esta-alavancando-o-crescimento-da-cloud/)
[Global Open Source Services Market (2021 to 2026)](https://finance.yahoo.com/news/global-open-source-services-market-094500559.html?guccounter=1&guce_referrer=aHR0cHM6Ly93d3cuZ29vZ2xlLmNvbS8&guce_referrer_sig=AQAAALNhEaJe700goKU_E7OvWQkb2d3CSWnK5qz03i4LLLKqlieRYxwslPkqxV494FM1K8z-pWAM44-I9HD0WZ-9M80rnkllxgh_phycu_66KzYTzeo-C_EpvpEFXUUxn-DwwRgqfqebKyBJCZpN4A-32BRP9VzxbxIMB6FwH_xKQaw6)
[Open Source: o crescimento sustentável no mercado da tecnologia](https://www.mjvinnovation.com/pt-br/blog/open-source/)
[Por que é importante que o poder público use software livre](https://brasil.elpais.com/brasil/2017/08/25/tecnologia/1503682398_611930.html)
